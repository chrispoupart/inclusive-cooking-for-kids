# Inclusive Cooking for Kids

A kid-friendly cookbook for kids with dietary restrictions.

The recipes in this book are designed to be used for cooking food for groups
of people, including children.  All too often, the people responsible for
preparing group food, be it for potlucks, trips, parties, or other events, do
not provide much consideration for kids who eat a different diet.

While we can't promise that this cookbook will help accommodate all different
dietary needs, we are hoping to make it as inclusive as possible, across the
board.

# Contributing

This project is licensed under a  <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>,
which means that you are free to re-use it however you like, so long as you
link back to us as the original source, and release your derivative work under
the same license.

Of course, we would love to integrate your work, or if that is not possible,
link to it as well!

If you would like to help out this project directly, we are always looking for
contributors, editors, and translators!

Shoot me an [email](mailto:chris.poupart@gmail.com) to get started, or send me
a merge request if you are comfortable with GitLab.
