---
title: German Lentil Soup
disqus: 'inclusive-cooking'
language: en
type: recipe
date: 2019-01-27T18:55:04.893Z
description: >-
  This is based on a soup that my Oma used to make growing up, and which my
  mother still makes.  It is hearty, satisfying, and easy to make.  The perfect
  combination.
author: Erika Podlech
serves: '20'
preptime: 10
preptime_units: minutes
cooktime: 30
cooktime_units: minutes
category: Hot Meals
ingredients:
  - ingredient: olive oil
    quantity: 2
    unit: tbsp(s)
  - ingredient: 'onions, chopped'
    quantity: 8
    unit: cup(s)
  - ingredient: 'carrots, chopped'
    quantity: 10
  - ingredient: 'brown or green lentils, rinsed and picked over'
    quantity: 6
    unit: cup(s)
  - ingredient: long grain brown rice
    quantity: 1
    unit: cup(s)
  - ingredient: 'bunches of fresh basil, finely chopped'
    quantity: 4
  - ingredient: red lentils
    quantity: 2
    unit: cup(s)
  - ingredient: salt
    quantity: 1.5
    unit: tbsp(s)
  - ingredient: water
    quantity: 6
    unit: litre(s)

instructions: |
  1. Heat the oil over medium heat in a large soup pot.

  1. Add the onion and saute until translucent, approximately 2 minutes.

  1. Add a little bit of water and de-glaze the pot. Then add the rest of the
     water and the remaining ingredients.

  1. Bring to a boil, then reduce the heat and cover, stirring occasionally,
     until lentils are cooked through, approximately 15 to 20 minutes.

  1. Add fresh pepper if desired, and serve.

allergens: ""

kidfriendly: true
---
{% include "recipe_template.md" %}
{% include "glossary.md" %}