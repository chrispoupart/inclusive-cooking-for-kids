---
title: Oatmeal
disqus: 'inclusive-cooking'
language: en
type: recipe
date: 2019-01-21T18:25:48.986Z
description: >-
  Inexpensive, hearty, and healthy. It's no wonder oatmeal has been a classic
  breakfast food since we began domesticating plants.  This recipe scales up
  easily to 100 or more servings.
author: Chris Poupart
serves: 20
preptime: 1
preptime_units: minutes
cooktime: 10
cooktime_units: minutes
category: Breakfast
ingredients:
  - ingredient: water
    quantity: 2
    unit: litres
  - ingredient: vanilla extract
    quantity: 3
    unit: tbsp
  - ingredient: maple syrup
    quantity: 3
    unit: tbsp
  - ingredient: salt
    quantity: 1.5
    unit: tsp
  - ingredient: rolled oats
    quantity: 10
    unit: cup(s)
  - ingredient: raisins (optional)
    quantity: 2
    unit: cup(s)
  - ingredient: shredded coconut (optional)
    quantity: 1.5
    unit: cup(s)
instructions: |
  1. Bring water to a boil in a large pot.

  1. Add remaining ingredients.

  1. Return to a boil, and turn heat to low. Stir often.

  1. Cook for 2 to 5 minutes.

  1. Remove from heat and serve with margarine, sweetener, and fresh fruits

allergens: ""
kidfriendly: true
---
{% include "recipe_template.md" %}
{% include "glossary.md" %}
