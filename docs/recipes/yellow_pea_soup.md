---
title: Yellow Pea Soup
disqus: 'inclusive-cooking'
language: en
type: recipe
date: 2019-01-27T16:35:38.523Z
description: >-
  This soup can be made with any bean substituted for the yellow peas; such as,
  black beans, lentils, split peas, etc., or any combination. For the grain,
  barley works best, but rice, whole oats, wheat berries, or another whole grain
  will work just as well.
author: Food Not Bombs
serves: '20'
preptime: 30
preptime_units: minutes
cooktime: 1
cooktime_units: hour
category: Hot Meals
ingredients:
  - ingredient: vegetable oil
    quantity: 1.5
    unit: tbsp(s)
  - ingredient: cloves of garlic
    quantity: 6
    unit: ''
  - ingredient: 'onions, chopped'
    quantity: 2
  - ingredient: dried thyme
    quantity: 0.5
    unit: tsp(s)
  - ingredient: dried basil
    quantity: 0.5
    unit: tsp(s)
  - ingredient: dried oregano
    quantity: 0.5
    unit: tsp(s)
  - ingredient: water
    quantity: 2.5
    unit: litre(s)
  - ingredient: yellow peas
    quantity: 2.5
    unit: cup(s)
  - ingredient: barley
    quantity: 0.75
    unit: cup(s)
  - ingredient: salt
    quantity: 2
    unit: tsp(s)
  - ingredient: 'potatoes, cubed'
    quantity: 2
    unit: []
  - ingredient: 'large carrots, chopped'
    quantity: 4
  - ingredient: 'stalks of celery, chopped'
    quantity: 6
instructions: |
  1. Heat oil in the bottom of soup pot. Saute garlic for 30 seconds, then add
     onions and spices. Saute until onions start to brown on edges.

  1. Add peas and stir until heated and coated with oil and spices; then add
     water, barley, salt, and pepper.

  1. Bring to a boil. Add chopped vegetables. Bring to a second boil, then
     reduce heat to low and cover. Stir occasionally. Simmer for 45 minutes or
     until peas are cooked to desired softness.

  **Note**: The soup can simmer for as long as you like, as long as you keep
  adding additional water. Serve hot.

allergens:
  - None

kidfriendly: true
---
{% include "recipe_template.md" %}
{% include "glossary.md" %}
