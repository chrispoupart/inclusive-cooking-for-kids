---
date: '2019-01-17 14:30:00 -0400'
title: Additional Considerations
---

# Additional Considerations

## Most Common Allergens

The following foods account for roughly 90% of all food allergies:

* Eggs
* Dairy
* Peanuts
* Crustaceans and molluscs
* Fish
* Tree nuts

Health Canada keeps a [list of priority allergens](https://www.canada.ca/en/health-canada/services/food-nutrition/food-safety/food-allergies-intolerances/food-allergies.html)
which expand on the above list to include:

* Mustard
* Sesame seeds
* Soy
* Sulphites
* Wheat and triticale

The recipes in this book will be free of all ingredients in the first list, they
will be labelled appropriately when they contain ingredients from the second
list.

As well, alcohol is a common ingredient in many cuisines, but similarly,
something that many people avoid for religious, ethical, or personal reasons.

If you use alcohol-based extracts (such as vanilla), or if your recipe calls for
alcohol such as beer or wine in the cooking process, please consider omitting
it, or if it must be used, clearly indicating it on your label. In the case of
extracts, you may also consider using non-alcohol-based extracts, or the natural
ingredient, such as vanilla beans instead of vanilla extract.

## Multiplying Recipes

We have tried to consistently make these recipes suitable for groups of roughly
20 people. If you need to scale up, or scale down, you can do so with relative
ease for most ingredients *except* salt and spices. Much less of both are needed
when you scale up, so if going from 20 to 100, you don't go from 1 tsp of salt
to 5 tsp of salt. Start low, and adjust to taste.

Similarly, prep work for large amounts is not significantly increased as the
recipes scale up. It is more efficient overall to prepare more of a single
ingredient. As such, it's worth looking at all of the food you will be
preparing, even if it will be over a few days, and consider doing the prep for
all the dishes at the same time, depending on your available storage space and
labour.

## Commonly Overlooked Ingredients

### For vegans, vegetarians, and plant-based eaters

* Margarine
    - Most commercial margarines contain dairy of one type or another, and other
      contain a form of vitamin D3 that is derived from animals.There are some
      brands that are suitable for vegans however, including "Becel Vegan",
      "Earth Balance", and "Melt". These are commonly available in grocery
      stores in Canada and the USA.
* Honey, bee pollen, honeycomb, beeswax, etc
    - Are derived from animals and not consumed by many people, including
      vegans, for ethical and environmental reasons.
* Gelatin
    - Examples include marshmallows, Jello, gummy candies, Mentos, etc.
* Fish
    - Examples include fish sauce, Worcestershire sauce, etc
* Meat-based broth and stocks (such as beef stock, or chicken stock)
* Insects
    - Many people don't realize that insects are a popular food additive, and
      there is a growing trend to use insects as a primary source of protein.
      These ingredients are not suitable for people who do not eat other
      animals.
    - Examples include: [Carmine, cochineal, cochineal extract, crimson lake, or
      E120](https://en.wikipedia.org/wiki/Carmine),
      [shellac](https://en.wikipedia.org/wiki/Shellac), and [cricket
      flour](https://en.wikipedia.org/wiki/Cricket_flour)

### For gluten-free diets

* Soy sauce
    - Soy sauce often has wheat as an ingredient. Consider using a product such
      as Braggs liquid aminos or tamari. Coconut aminos also work but are much
      harder to come by.  However, they are also suitable for people with soy
      allergies.
* Commercial broth and bouillons.
    - Again, wheat is a common ingredient in these products. Look for certified
      gluten free bouillons, or make your own with herbs and salt.

{% include "glossary.md" %}