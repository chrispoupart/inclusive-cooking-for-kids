---
date: '2019-01-17 14:30:00 -0400'
title: Introduction
description: A guide for cooking inclusive food for group events.
---
![Cover Image - A "Food Not Bombs" logo of a purple fist holding an orange carrot.](/images/fnb.png "This Food Not Bombs logo was designed by Keith McHenry in 1981. It represents liberty, inclusivity, and strength through unity.")
## Why did we create this book?

No one likes to be left out, yet as adults organizing food for social events, we
do it _all the time_. Parents of kids with allergies, or with vegetarian or
vegan children know what I am talking about.

It's isn't malicious. For many, it's simply not something that they think about.
For others, they don't know where to start, or how to prepare truly inclusive
food.

## What do we mean by "inclusive food"?

Inclusive food is literally food that is suitable to the widest possible number
of people.  This book will include tips and guidelines, as well as recipes that
will help you to create food that can be eaten by almost everyone.

In short, that means avoiding the most common food allergens, as well as meat
(including fowl and fish) and clearly labelling foods so that people can make
informed decisions about what to eat.

## Reasons people eat differently.

* Allergies
* Food intolerances
* Ethics
* Religion
* Environmental considerations
* Other?

## License and Reproduction

The [Food Not Bombs](http://foodnotbombs.net/)
[logo](http://foodnotbombs.net/logo.html) was created by Keith McHenry and
released to the Public Domain.

All attributed work is copyrighted by the person or organisation to whom it is
attributed.

All unattributed work is the work of Circle Eh.

{% include "creativecommons.md" %}
