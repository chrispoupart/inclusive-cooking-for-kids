<!-- These entries are duplicated because the attr plugin is case sensitive -->
*[Margarine]: Most commercial margarines contain dairy of one type or another, and other contain a form of vitamin D3 that is derived from animals.There are some brands that are suitable for vegans however, including "Becel Vegan", "Earth Balance", and "Melt". These are commonly available in grocery stores in Canada and the USA.  If there is concern about soy allergies, consider using a liquid neutral oil instead of margarine.
*[margarine]: Most commercial margarines contain dairy of one type or another, and other contain a form of vitamin D3 that is derived from animals.There are some brands that are suitable for vegans however, including "Becel Vegan", "Earth Balance", and "Melt". These are commonly available in grocery stores in Canada and the USA.  If there is concern about soy allergies, consider using a liquid neutral oil instead of margarine.
*[Sesame seeds]: A potential allergen. Use with caution.
*[sesame seeds]: A potential allergen. Use with caution.
*[Mustard]: A potential allergen. Use with caution.
*[mustard]: A potential allergen. Use with caution.
*[Soy milk]: Soy is a potential allergen. If this is a concern for your group, consider using rice, oat, or hemp milk.
*[soy milk]: Soy is a potential allergen. If this is a concern for your group, consider using rice, oat, or hemp milk.
*[Soy]: A potential allergen. Use with caution.
*[soy]: A potential allergen. Use with caution.
*[Sulphites]: A potential allergen. Use with caution.
*[sulphites]: A potential allergen. Use with caution.
*[Wheat]: A potential allergen. Use with caution.
*[wheat]: A potential allergen. Use with caution.
*[Wheat Flour]: Wheat flour is a potential allergen, and may cause problems for people with intolerances. Consider using a certified gluten-free flour when possible, and be sure to include it as an ingredient on the display label.
*[wheat flour]: Wheat flour is a potential allergen, and may cause problems for people with intolerances. Consider using a certified gluten-free flour when possible, and be sure to include it as an ingredient on the display label.
